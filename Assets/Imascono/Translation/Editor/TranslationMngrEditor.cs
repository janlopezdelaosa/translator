﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace ICTranslation
{
    [CustomEditor(typeof(TranslationMngr))]
    public class TranslationMngrEditor : Editor
    {
        private const string lblButtonGenerateLanguageFile = "Generate languages file";
        private const string lblButtonGenerateSceneTranslationItems = "Generate & attach scene translation items";

        public override void OnInspectorGUI()
        {
            TranslationMngr tm = (TranslationMngr)target;

            if (GUILayout.Button(lblButtonGenerateLanguageFile))
            {
                tm.fileProps.ResetProperties();
                tm.UpdateLanguagesFile();
                EditorUtility.SetDirty(tm);
            }

            if (GUILayout.Button(lblButtonGenerateSceneTranslationItems))
            {
                tm.fileProps.ResetProperties();
                tm.GenerateAndAttachSceneTranslationItems();
                EditorUtility.SetDirty(tm);

                if (tm.dropdown)
                {
                    List<LanguageType>lstSupportedLangs = tm.GetSupportedLanguages();

                    List<Dropdown.OptionData> lstDropdown = new List<Dropdown.OptionData>();
                    int value = 0;
                    for (int i = 0; i < lstSupportedLangs.Count; i++)
                    {
                        if (lstSupportedLangs[i] == tm.lang) value = i;
                        lstDropdown.Add(new Dropdown.OptionData(lstSupportedLangs[i].ToString()));
                    }
                    tm.dropdown.options = lstDropdown;
                    tm.dropdown.value = value;
                    EditorUtility.SetDirty(tm.dropdown);
                }
            }

            base.DrawDefaultInspector();
        }

    }
}