﻿
namespace ICTranslation
{
    public class XDef
    {
        public const string TRANS = "translations";
        public const string NAME = "name";
        public const string ID = "id";
        public const string TYPE = "type";
        public const string LANG = "lang";
        public const string LANGS = "langs";
        public const string LANG_FROM = "from";
        public const string LANG_TO = "to";
    }

    /* languages.xml
      
     Description:

     languages.xml is an XDocument made of a translation
     A translation is an XElement made of scenes
         Translation Attribute's: langs
             Lang attribute value: (string[] of) LanguageType
     A scene is an XElement made of items
         Scene Attribute's: name
             Name attribute value: string
     An item is an XElement made of contents
         Item Attribute's: type, id, name
             Type attribute value: (string of) {txt, img, vid}
             ID attribute value: integer
             Name attribute value: string
     A content is an XElement
         Content Attribute's: lang
             Lang attribute value: (string of) LanguageType
         Content value: string

     Example:

     <translations langs = "en, fr" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
       <scene name = "0_Splash">
         <item type="txt" id ="1" name="TxtA">
           <content lang="en">This is text A from generic_app Splash screen</content>
           <content lang="fr">Ça c'est le text A de lécran splash de l'app générale</content>
         </item>
         <item type="img" id ="3" name="ImgAbout">
           <content lang="en">chromEn</content>
           <content lang="fr">chromFr</content>
         </item>
       </scene>
     </translations>
    */

    /* languages_A_B.xml
    
     Description:

     languages_A_B.xml is an XDocument made of a translation
     A translation is an XElement made of scenes
         Translation Attribute's: from, to
             From attribute value: A ((string of) LanguageType)
             To attribute value: B ((string of) LanguageType)
     A scene is an XElement made of items
         Scene Attribute's: name
             Name attribute value: string
     An item is an XElement
         Item Attribute's: id, name
             Name attribute value: string
             ID attribute value: integer
         Item value: string

     Example:

     <translations from = "en" to = "es" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
       <scene name = "0_Splash">
         <item name="TxtA" id ="This is text A from generic_app Splash screen">Este es el texto A de la pantalla Splash de la app genérica
         </item>
         <item name = "TxtB" id="This is text B from generic_app Splash screen">Este es el texto B de la pantalla Splash de la app genérica
         </item>
       </scene>
       <scene name = "1_AppActivation">
         <item name="TxtInfo" id ="Please enter the password to find out more about feline hypertension and enjoy augmented reality" > Introduzca la contraseña para descubrir más acerca de la hipertensión felina y disfrutar de una experiencia de realidad aumentada
         </item>
         <item name = "Placeholder" id="Enter code" >Introduzca la contraseña
         </item>
         <item name = "TxtValidate" id="Validate">Comprobar contraseña
         </item>
         <item name = "LblErrorCode" id = "Validation code not valid" > La contraseña no es válida
         </item>
       </scene>
     </translations>
    */

}
