﻿
namespace ICTranslation
{
    public abstract class Item<T> : Item
    {
        protected abstract T GetContent(LanguageType lang);
    }
}