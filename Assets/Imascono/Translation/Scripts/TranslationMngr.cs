﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Video;

namespace ICTranslation
{
    public enum LanguageType { lat, en, fr, es };

    [ExecuteInEditMode]
    public class TranslationMngr : MonoBehaviour
    {
        // Singleton: ref to the only possible instantiated object
        public static TranslationMngr Instance { get; private set; }

        public static LanguageType language = LanguageType.en;
        public LanguageType lang = LanguageType.en;             // TODO: si sólo gano ver el idioma en el Inspector... ponerlo como una label en Editor y ya

        public Dropdown dropdown;
        private Dictionary<int, LanguageType> d_languages;
        private Dictionary<int, LanguageType> D_languages
        {
            get
            {
                if (d_languages == null)
                {
                    List<LanguageType> lstSupportedLangs = GetSupportedLanguages();
                    d_languages = new Dictionary<int, LanguageType>();

                    for (int i = 0; i < lstSupportedLangs.Count; i++)
                        d_languages.Add(i, lstSupportedLangs[i]);
                }
                return d_languages;
            }
        }

        public Item[] _items;
        public static Item[] Items
        {
            get { return Instance._items; }
        }

        private const string LANG_FOLDER = "Translation";
        private const string LANG_XML_FILE = "languages.xml";
        private const string LANG_XML_FILES = "languages_*.xml";
        private const string STREAMING_ASSETS_FOLDER = "StreamingAssets";
        private const string ASSETS = "Assets";

        // In editor mode references won't be lost from one execution to the other
        // So LoadSceneLanguages needs to reset references as if a new Start was being executed
        public class PropertiesReset
        {
            public bool DoLangFileNameReset { get; private set; }
            public bool DoLangXMLReset { get; private set; }
            public bool DoLangFileNamesReset { get; private set; }

            public PropertiesReset() { ResetProperties(); }

            public void LangFileNameSet() { DoLangFileNameReset = false; }
            public void LangXMLSet() { DoLangXMLReset = false; }
            public void LangFileNamesSet() { DoLangFileNamesReset = false; }

            public void ResetProperties()
            {
                DoLangFileNameReset = true;
                DoLangXMLReset = true;
                DoLangFileNamesReset = true;
            }
        }

        public PropertiesReset fileProps = new PropertiesReset();

        private string _langFileName = String.Empty;
        private string LangFileName
        {
            get
            {
                if (String.IsNullOrEmpty(_langFileName) || fileProps.DoLangFileNameReset)
                    _langFileName = BetterStreamingAssets.GetFiles(LANG_FOLDER, LANG_XML_FILE)[0];

                fileProps.LangFileNameSet();
                return _langFileName;
            }
        }

        private XDocument _langXml;
        private XDocument LangXml
        {
            get
            {
                if (_langXml == null || fileProps.DoLangXMLReset)
                    _langXml = ReadFromXml(LangFileName);

                fileProps.LangXMLSet();
                return _langXml;
            }
        }

        private string[] _langsFileNames = null;
        private string[] LangsFileNames
        {
            get
            {
                if (_langsFileNames == null || _langsFileNames.Length == 0 || fileProps.DoLangFileNamesReset)
                    _langsFileNames = BetterStreamingAssets.GetFiles(LANG_FOLDER, LANG_XML_FILES);

                fileProps.LangFileNamesSet();
                return _langsFileNames;
            }
        }

        void Start()                                             // Needs AppManager already awaken ¿????? WHY ?
        {
            BetterStreamingAssets.Initialize();

            if (Instance == null)
            {
                print("null");
                language = lang;
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
            else if (Instance != this)
            {
                print("not null");
                int n = this._items.Length;
                Instance._items = new Item[this._items.Length];
                for (int i = 0; i < this._items.Length; i++)      // Copy THIS object's translationElmnts
                    Instance._items[i] = this._items[i];          // ...to INSTANCE's

                DestroyImmediate(gameObject);
            }
            else
            {
                Debug.Log("Instance not null and equal to this");
            }

            // AppManager.Instance.CurrentScene can't be used because 
            // scene "6_Loading" never enters the sceneHistory stack
            LoadSceneLanguages(SceneManager.GetActiveScene().name);
            TranslateScene();
            UpdateLanguagesFile();
        }

        public void LoadSceneLanguages(string currentScene, params string[] langs)
        {
            IEnumerable<XElement> xItems =
                from xScene in LangXml.Elements(XDef.TRANS).Elements()
                where xScene.Attribute(XDef.NAME).Value == currentScene
                from xItem in xScene.Elements()
                select xItem;

            foreach (XElement xItem in xItems)
            {
                //Item.TranslationItemType type = (Item.TranslationItemType)Enum.Parse(typeof(Item.TranslationItemType), xItem.Attribute(XDef.TYPE).Value);
                int id = int.Parse(xItem.Attribute(XDef.ID).Value);
                //string name = xItem.Attribute(XDef.NAME).Value;

                Item item = (Item)Items.Where(i => i.id == id).ToArray()[0];
                item.d_string = new Dictionary<LanguageType, string>();

                foreach (XElement xContent in xItem.Elements())
                {
                    LanguageType lang = (LanguageType)Enum.Parse(typeof(LanguageType), xContent.Attribute(XDef.LANG).Value);
                    item.d_string.Add(lang, xContent.Value);
                }
            }
        }

        public List<LanguageType> GetSupportedLanguages()
        {
            string supportedLangsAsAttributeValue =
                (from xTranslation in LangXml.Elements(XDef.TRANS)
                 select xTranslation).ToArray()[0].Attribute(XDef.LANGS).Value;

            string[] arrSupportedLangs = supportedLangsAsAttributeValue.Split(',').Select(x => x.Trim()).ToArray();

            List<LanguageType> lstSupportedLangs = new List<LanguageType>();

            foreach (string sl in arrSupportedLangs)
            {
                LanguageType l;
                if (!Enum.TryParse(sl, out l))
                {
                    print(string.Format(
                        "Error in file {0}. Supported language {1} unknown",
                        LangFileName, sl));
                    continue;
                }
                lstSupportedLangs.Add(l);
            }

            return lstSupportedLangs;
        }

        public void UpdateLanguagesFile()
        {
            BetterStreamingAssets.Initialize();

            //string supportedLangsAsAttributeValue =
            //    (from xTranslation in LangXml.Elements(XDef.TRANS)
            //     select xTranslation).ToArray()[0].Attribute(XDef.LANGS).Value;

            //string[] arrSupportedLangs = supportedLangsAsAttributeValue.Split(',').Select(x => x.Trim()).ToArray();

            //List<LanguageType> lstSupportedLangs = new List<LanguageType>();

            //foreach (string sl in arrSupportedLangs)
            //{
            //    LanguageType l;
            //    if (!Enum.TryParse(sl, out l))
            //    {
            //        print(string.Format(
            //            "Error in file {0}. Supported language {1} unknown",
            //            LangFileName, sl));
            //        continue;
            //    }
            //    lstSupportedLangs.Add(l);
            //}

            List<LanguageType> lstSupportedLangs = GetSupportedLanguages();

            string debug = "Supported languages: ";
            foreach (LanguageType l in lstSupportedLangs) debug += l.ToString() + " ";
            print(debug);

            foreach (string lfn in LangsFileNames)
            {
                string[] langsInFileName = lfn.Split('.')[0].Split('_').Skip(1).ToArray();

                debug = "Langs in language file name: ";
                foreach (string l in langsInFileName) debug += l.ToString() + " ";
                print(debug);

                XDocument langXml = ReadFromXml(lfn);

                XElement[] xTranslations =
                    (from xTranslation in langXml.Elements(XDef.TRANS)
                     select xTranslation).ToArray();

                string[] langsInXmlFile = new string[2]
                {
                    xTranslations[0].Attribute(XDef.LANG_FROM).Value,
                    xTranslations[0].Attribute(XDef.LANG_TO).Value
                };

                debug = "Langs in xml attributes: ";
                print(debug + " " + xTranslations[0].Attribute(XDef.LANG_FROM).Value + " " + xTranslations[0].Attribute(XDef.LANG_TO).Value);

                if ((langsInFileName[0] != langsInXmlFile[0]) ||
                    (langsInFileName[1] != langsInXmlFile[1]))
                {
                    print(string.Format(
                        "Confusion regarding the languages of file {0}." +
                        "\nFile name suggests translation from {1} to {2} but" +
                        "\ntags from xml suggest translation from {3} to {4}",
                        lfn, langsInFileName[0], langsInFileName[1],
                        langsInXmlFile[0], langsInXmlFile[1]));

                    continue;
                }

                LanguageType lFrom;
                LanguageType lTo;
                bool isUpdating = false;

                if (!Enum.TryParse(langsInFileName[0], out lFrom))
                {
                    print(string.Format(
                        "Error in file {0}. \"From\" language {1} unknown",
                        lfn, langsInFileName[0]));
                }
                if (!Enum.TryParse(langsInFileName[1], out lTo))
                {
                    print(string.Format(
                        "Error in file {0}. \"To\" language {1} unknown",
                        lfn, langsInFileName[1]));
                    continue;
                }

                if (!lstSupportedLangs.Contains(lFrom))
                {
                    print(string.Format(
                        "Can't include translation from file {0}." +
                        "/n\"From\" language {1} not yet included in languages.xml",
                        lfn, lFrom));
                    continue;
                }
                if (lstSupportedLangs.Contains(lTo))
                {
                    isUpdating = true;
                    print(string.Format(
                        "\"To\" language {1} already included in file {0}" +
                        "\nlanguages.xml info for language {1} will be updated",
                        lfn, lTo));
                }

                XElement[] xScenes_L =
                    (from xScene in LangXml.Elements(XDef.TRANS).Elements()
                     select xScene).ToArray();

                XElement[] xScenes_l =
                    (from xScene in langXml.Elements(XDef.TRANS).Elements()
                     select xScene).ToArray();

                if (xScenes_L.Length != xScenes_l.Length)
                {
                    print(string.Format(
                        "File {0} has {1} scenes.\n" +
                        "File {2} has {3} scenes.  " +
                        "\nNumber of scenes must be the same",
                        LangFileName, xScenes_L.Length,
                        lfn, xScenes_l.Length));
                }

                for (int i = 0; i < xScenes_l.Length; i++)
                // (xScenes_l.Length == xScenes_L.Length)
                {
                    if (xScenes_L[i].Attribute(XDef.NAME).Value !=
                        xScenes_l[i].Attribute(XDef.NAME).Value)
                    {
                        print(string.Format(
                            "Scene {0} has different value for name property:\n" +
                            "In file {1} it's called {2}." +
                            "In file {2} it's called {3}." +
                            "\nName of scenes must be the same",
                            i,
                            LangFileName, xScenes_L[i].Attribute(XDef.NAME).Value,
                            lfn, xScenes_l[i].Attribute(XDef.NAME).Value));
                    }

                    XElement[] xItems_L =
                        (from xItem in xScenes_L[i].Elements()
                         //where xItem.Attribute(XDef.TYPE).Value == Item.TranslationItemType.txt.ToString()
                         select xItem).ToArray();

                    //print("L length: " + xItems_L.Length);

                    XElement[] xItems_l =
                        (from xItem in xScenes_l[i].Elements()
                         select xItem).ToArray();

                    foreach (XElement xE in xItems_L) print(xE.Attribute(XDef.NAME).Value);
                    foreach (XElement xE in xItems_l) print(xE.Attribute(XDef.NAME).Value);

                    //print("l length: " + xItems_l.Length);

                    if (xItems_L.Length != xItems_l.Length)
                    {
                        print(string.Format(
                            "Scene {0} (\"{1}\") from file {2} has {3} items.\n" +
                            "Scene {4} (\"{5}\") from file {6} has {7} items.\n" +
                            "\nNumber of items per scene must be the same",
                            i, xScenes_L[i].Attribute(XDef.NAME).Value, LangFileName, xItems_L.Length,
                            i, xScenes_l[i].Attribute(XDef.NAME).Value, lfn, xItems_l.Length));
                    }

                    for (int j = 0; j < xItems_l.Length; j++)
                    // (xItems_l.Length == xItems_L.Length)
                    {
                        if (xItems_L[j].Attribute(XDef.NAME).Value !=
                            xItems_l[j].Attribute(XDef.NAME).Value)
                        {
                            print(string.Format(
                                "Item {0} of scene {1} (\"{2}\") has different value for name property:\n" +
                                "In file \"{3}\" it's called \"{4}\"." +
                                "In file \"{5}\" it's called \"{6}\"." +
                                "\nName of items in a scene must be the same",
                                j, i, xScenes_L[i].Attribute(XDef.NAME).Value,
                                LangFileName, xItems_L[j].Attribute(XDef.NAME).Value,
                                lfn, xItems_l[j].Attribute(XDef.NAME).Value));
                        }

                        XElement xContent_L_lFrom =
                            (from xContent in xItems_L[j].Elements()
                             where xContent.Attribute(XDef.LANG).Value == lFrom.ToString()
                             select xContent).ToArray()[0];

                        string content_L_lFrom = xContent_L_lFrom.Value;

                        XElement xItem_l =
                            (from xItem in xItems_l
                             where xItem.Attribute(XDef.NAME).Value ==
                                   xItems_L[j].Attribute(XDef.NAME).Value
                             select xItem).ToArray()[0];

                        string IDValue = xItem_l.Attribute(XDef.ID).Value;
                        string processedIDValue = xItem_l.Attribute(XDef.ID).Value.Replace("\\n", "\n");

                        if (processedIDValue != content_L_lFrom)
                        {
                            Debug.LogWarning(string.Format(
                                "Item {0} of scene {1} (\"{2}\") can't be translated:\n" +
                                "ID in file \"{3}\" is called \"{4}\"\n" +
                                "but processed value of content lang {5} in file \"{6}\" is: \"{7}\"." +
                                "\n{8}\nThe strings values must must be the same\n",
                                j, i, xScenes_L[i].Attribute(XDef.NAME).Value,
                                lfn, processedIDValue,
                                lFrom, LangFileName, content_L_lFrom, IDValue));
                        }
                        else
                        {
                            print(string.Format(
                                "Scene {0}, item {1}\n" +
                                "From {2}: {3}\n" +
                                "To {4}: {5}\n",
                                xScenes_L[i].Attribute(XDef.NAME).Value, xItem_l.Attribute(XDef.NAME).Value,
                                lFrom, content_L_lFrom,
                                lTo, xItem_l.Value));

                            if (isUpdating)
                            {
                                XElement xContent_L_lTo =
                                    (from xContent in xItems_L[j].Elements()
                                     where xContent.Attribute(XDef.LANG).Value == lTo.ToString()
                                     select xContent).ToArray()[0];
                                xContent_L_lTo.Value = xItem_l.Value;
                            }
                            else
                                xItems_L[j].Add(
                                    new XElement("content", new XAttribute("lang", lTo.ToString()), xItem_l.Value));
                        }
                    }
                }

                if (!isUpdating)
                {
                    LangXml.Elements(XDef.TRANS).ToArray()[0].Attribute(XDef.LANGS).Value += string.Format(", {0}", lTo.ToString());
                }
            }

            //LangXml.Save(@"C:\Users\juan.lopez\Desktop\languages2.xml");
            LangXml.Save(Path.Combine(ASSETS, STREAMING_ASSETS_FOLDER, LANG_FOLDER, "languages2.xml"));
            //print("XML saved to \"C:\\Users\\juan.lopez\\Desktop\\languages2.xml\"");
            print("XML saved to " + Path.Combine(ASSETS, STREAMING_ASSETS_FOLDER, LANG_FOLDER, "languages2.xml"));
        }

        public void GenerateAndAttachSceneTranslationItems()
        {
            BetterStreamingAssets.Initialize();

            IEnumerable<XElement> xItems =
                from xScene in LangXml.Elements(XDef.TRANS).Elements()
                where xScene.Attribute(XDef.NAME).Value == SceneManager.GetActiveScene().name // currentScene
                from xItem in xScene.Elements()
                select xItem;

            _items = new Item[xItems.ToArray().Length];
            int i = -1;

            foreach (XElement xItem in xItems)
            {
                string itemName = xItem.Attribute(XDef.NAME).Value;

                int itemID;
                if (!Int32.TryParse(xItem.Attribute(XDef.ID).Value, out itemID))
                {
                    print(string.Format(
                        "Error in item {0}. ID \"{1}\" must be an integer",
                        xItem.Attribute(XDef.NAME).Value,
                        xItem.Attribute(XDef.ID).Value));
                    continue;
                }

                Item.TranslationItemType itemType;
                if (!Enum.TryParse(xItem.Attribute(XDef.TYPE).Value, out itemType))
                {
                    print(string.Format(
                        "Error in item {0}. Type {1} unknown",
                        xItem.Attribute(XDef.NAME).Value,
                        xItem.Attribute(XDef.TYPE).Value));
                    continue;
                }

                print(itemName + " of type " + itemType + " and id " + itemID);

                Item item = GameObject.Find(xItem.Attribute(XDef.NAME).Value).GetComponent<Item>();

                switch (itemType)
                {
                    case Item.TranslationItemType.txt:
                        if (item == null)
                            item = GameObject.Find(xItem.Attribute(XDef.NAME).Value).AddComponent<ItemString>();
                        Text auxText = item.GetComponent<Text>();
                        ((ItemString)(item)).text = auxText;
                        ((ItemString)(item)).id = itemID;
                        break;

                    case Item.TranslationItemType.img:
                        if (item == null)
                            item = GameObject.Find(xItem.Attribute(XDef.NAME).Value).AddComponent<ItemSprite>();
                        Image auxImage = item.GetComponent<Image>();
                        ((ItemSprite)(item)).image = auxImage;
                        ((ItemSprite)(item)).id = itemID;
                        break;

                    case Item.TranslationItemType.vid:
                        if (item == null)
                            item = GameObject.Find(xItem.Attribute(XDef.NAME).Value).AddComponent<ItemVideo>();
                        VideoPlayer auxVideoPlayer = item.GetComponent<VideoPlayer>();
                        ((ItemVideo)(item)).videoPlayer = auxVideoPlayer;
                        ((ItemVideo)(item)).id = itemID;
                        Transform rawImageGO = item.transform.parent.Find("RawImage");
                        if (rawImageGO == null)
                        {
                            Debug.LogWarning(string.Format(
                            "Couldn't finish setting ItemVideo {0}. " +
                            "GO needs a brother named \"RawImage\"",
                            item.transform.name));
                        }
                        else
                        {
                            RawImage rawImg = rawImageGO.GetComponent<RawImage>();
                            if (rawImg == null)
                                rawImg = rawImageGO.gameObject.AddComponent<RawImage>();
                            ((ItemVideo)(item)).rawImage = rawImg;
                        }
                        break;
                }
                _items[++i] = item;
            }
        }

        private XDocument ReadFromXml(string path)
        {
            if (!BetterStreamingAssets.FileExists(path))
            {
                Debug.LogErrorFormat("Streaming asset not found: {0}", path);
                return null;
            }

            using (Stream stream = BetterStreamingAssets.OpenRead(path))
            {
                return XDocument.Load(stream);
            }
        }

        public void TranslateScene()
        {
            foreach (Item item in Items)
                item.UpdateValue(language);
        }

        public void SetLanguage(LanguageType lang)
        {
            //Instance.lang = lang;
            language = lang;
        }

        public void SetLanguage(string strLang)
        {
            try
            {
                LanguageType lang = (LanguageType)Enum.Parse(typeof(LanguageType), strLang);
                //Instance.lang = lang;
                SetLanguage(lang);
                //language = lang;
            }
            catch (Exception e)
            {
                Debug.LogError("\"" + strLang + "\" language not recognised.\n" + e.Message);
            }
        }

        public void ChangeLanguageValue(bool val)
        {
            SetLanguage((val) ? LanguageType.fr : LanguageType.en);
            //language = (val) ? LanguageType.fr : LanguageType.en;
            //Instance.lang = language;
            TranslateScene();
        }

        public void ChangeLanguageValue(int val)
        {
            SetLanguage(D_languages[val]);

            TranslateScene();
        }
        //ktd
    }
}
