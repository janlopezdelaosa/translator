﻿using UnityEngine;
using UnityEngine.UI;

namespace ICTranslation
{
    [RequireComponent(typeof(Text))]
    public class ItemString : Item<string>
    {
        public override TranslationItemType Type
        {
            get { return TranslationItemType.txt; }
        }

        public Text text;

//#if UNITY_EDITOR
//        /////////////////////////////////////////////////////////////////////
//        [Serializable]
//        public class Serializabledict { public string key, value; }
//        public Serializabledict[] chanodict;

//        public void ShowContents()
//        {
//            chanodict = new Serializabledict[d_contents.Keys.Count];
//            int i = 0;
//            foreach (KeyValuePair<LanguageType, string> kv in d_contents)
//            {
//                chanodict[i] = new Serializabledict
//                {
//                    key = kv.Key.ToString(),
//                    value = kv.Value
//                };
//                i++;
//            }
//        }
//        /////////////////////////////////////////////////////////////////////
//#endif


        protected override string GetContent(LanguageType lang)
        {
            return d_string[lang];

            //string val = "";
            //if (!d_string.TryGetValue(lang, out val))
            //    Debug.LogError("Value from dict couldn't be retrieved");
            //return val;
        }

        public override void UpdateValue(LanguageType lang)
        {
            text.text = GetContent(lang);
        }
    }
}
