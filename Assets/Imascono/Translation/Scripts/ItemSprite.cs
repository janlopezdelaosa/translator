﻿using UnityEngine;
using UnityEngine.UI;

namespace ICTranslation
{
    [RequireComponent(typeof(Sprite))]
    public class ItemSprite : Item<Sprite>
    {
        public override TranslationItemType Type
        {
            get { return TranslationItemType.img; }
        }

        public Image image;

        //#if UNITY_EDITOR
        ///////////////////////////////////////////////////////////////////////
        //[Serializable]
        //public class Serializabledict { public string key; public Sprite value; }
        //public Serializabledict[] chanodict;

        //public void ShowContents()
        //{
        //    chanodict = new Serializabledict[d_contents.Keys.Count];
        //    int i = 0;
        //    foreach (KeyValuePair<LanguageType, Sprite> kv in d_contents)
        //    {
        //        chanodict[i] = new Serializabledict
        //        {
        //            key = kv.Key.ToString(),
        //            value = kv.Value
        //        };
        //        i++;
        //    }
        //}
        ///////////////////////////////////////////////////////////////////////
        //#endif

        //private Dictionary<LanguageType, Sprite> d_contents;
        //private Dictionary<LanguageType, Sprite> D_contents
        //{
        //    get
        //    {
        //        if (d_contents == null)
        //            d_contents = new Dictionary<LanguageType, Sprite>();
        //        return d_contents;
        //    }
        //}
        //protected override Sprite GetContent(LanguageType lang)
        //{
        //    Sprite s;
        //    if (D_contents.TryGetValue(lang, out s))
        //        return s;
        //    else
        //        D_contents.Add(lang, Resources.Load<Sprite>(d_string[lang]));

        //    return D_contents[lang];
        //}

        protected override Sprite GetContent(LanguageType lang)
        {
            return Resources.Load<Sprite>(d_string[lang]);
        }
        
        public override void UpdateValue(LanguageType lang)
        {
            image.sprite = GetContent(lang);
        }
    }
}
