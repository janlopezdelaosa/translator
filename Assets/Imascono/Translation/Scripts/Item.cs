﻿using System.Collections.Generic;
using UnityEngine;

namespace ICTranslation
{
    // Con un interface valdría pero entonces no podría 
    // asignar los TranslationItemString y TranslationItemSprite
    // al array TranslationItem[] en TranslationMngr
    //public interface ITranslationItem
    //{
    //    // empty interface so that TranslationItem<T>s
    //    // of different types T can be added in the
    //    // same structures in TranslationMngr
    //}

    public abstract class Item : MonoBehaviour
    {
        public enum TranslationItemType { txt, img, vid };
        public virtual TranslationItemType Type { get; set; }

        public int id;

        public string Name { get { return transform.name; } }

        public Dictionary<LanguageType, string> d_string;
        public abstract void UpdateValue(LanguageType lang);
    }
}