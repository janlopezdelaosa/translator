﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace ICTranslation
{
    [RequireComponent(typeof(VideoPlayer))]
    public class ItemVideo : Item<VideoClip>
    {
        public override TranslationItemType Type
        {
            get { return TranslationItemType.vid; }
        }

        public VideoPlayer videoPlayer;
        public RawImage rawImage;

        //#if UNITY_EDITOR
        ///////////////////////////////////////////////////////////////////////
        //[Serializable]
        //public class Serializabledict { public string key; public VideoClip value; }
        //public Serializabledict[] chanodict;

        //public void ShowContents()
        //{
        //    chanodict = new Serializabledict[d_contents.Keys.Count];
        //    int i = 0;
        //    foreach (KeyValuePair<LanguageType, VideoClip> kv in d_contents)
        //    {
        //        chanodict[i] = new Serializabledict
        //        {
        //            key = kv.Key.ToString(),
        //            value = kv.Value
        //        };
        //        i++;
        //    }
        //}
        ///////////////////////////////////////////////////////////////////////
        //#endif

        //private Dictionary<LanguageType, VideoClip> d_contents;
        //private Dictionary<LanguageType, VideoClip> D_contents
        //{
        //    get
        //    {
        //        if (d_contents == null)
        //            d_contents = new Dictionary<LanguageType, VideoClip>();
        //        return d_contents;
        //    }
        //}
        //protected override VideoClip GetContent(LanguageType lang)
        //{
        //    VideoClip vc;

        //    if (D_contents.TryGetValue(lang, out vc))
        //        return vc;
        //    else
        //        D_contents.Add(lang, Resources.Load<VideoClip>(d_string[lang]));

        //    return D_contents[lang];
        //}

        protected override VideoClip GetContent(LanguageType lang)
        {
            return Resources.Load<VideoClip>(d_string[lang]);
        }

        public override void UpdateValue(LanguageType lang)
        {
            videoPlayer.clip = GetContent(lang);

            videoPlayer.Play();
            StartCoroutine(EnableRawImageAfterSeconds(1f));
        }

        private IEnumerator EnableRawImageAfterSeconds(float t)
        {
            yield return new WaitForSeconds(t);
            rawImage.gameObject.SetActive(true);
        }
    }
}
