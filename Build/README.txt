///////////////////////////////////////////////////////////////////////////////
                               IC_Translation
///////////////////////////////////////////////////////////////////////////////

IC_Translation_v1:

 /Imascono                           | Imascono's plugins folder
   /Translation                      | Translation plugin's folder
     /Editor                         |
       � TranslationMngrEditor.cs    | Custom TranslatorMngr editor
     /Prefabs                        |
       � TranslationDropdown.prefab  | Dropdown to change languages
       � TranslationMngr.prefab      | TranslatorMngr gameobject
     /Scripts                        |
       � Item.cs                     | Base Item
       � ItemGeneric.cs              | Generic Item
       � ItemSprite.cs               | Image
       � ItemString.cs               | Text
       � ItemVideo.cs                | Video
       � TranslationMngr.cs          | Manager
       � XDef.cs                     | Definitions
                                     
 /Plugins                            |
   /BetterStreamingAssets            | 3rd party plugin to read files
                                     
 /Resources                          | 
   /Translation                      | Image & video resources folder
     /SamplesScene                   | 
       � Images...                   |
       � Videos...                   |
                                     
 /StreamingAssets                    |
   /Translation                      | Language xml files
     � languages.xml                 | Main translation file
     � languages_X_Y.xml             | Translations from lang X to Y
     � languages_X_Z.xml             |
     � ...                           
                                     
 /Textures                           |
   /SampleScene                      |
     � TranslationVideoRenderTexture | RenderTexture for VideoPlayer

///////////////////////////////////////////////////////////////////////////////